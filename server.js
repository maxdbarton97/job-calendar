
// ============================================================================
// VARIABLES
// ============================================================================

// dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session =  require('express-session');



//const axios = require('axios')

// MongoDB and Mongoose
const url = 'mongodb://mbarton:abcdef1@ds151461.mlab.com:51461/job-calendar-development'
mongoose.connect(url).then(suc => {
  
  // axios.post('http://localhost:8080/auth/register', {
  //   email: 'maxdbarton97@gmail.com',
  //   forename: 'Bob',
  //   surnames: 'Ross',
  //   username: 'bob_ross27',
  //   password: 'paintme123'
  // });

  // axios.post('http://localhost:8080/auth/register/email-check', {
  //   email: 'maxdbarton97@gmail.com',
  // }).then(res => console.log(res.data)).catch(err => console.log(err));

  // axios.post('http://localhost:8080/auth/register/username-check', {
  //   username: 'bob_ross27',
  // }).then(res => console.log(res.data)).catch(err => console.log(err));


  // axios.post('http://localhost:8080/auth/register/username-check', {
  //   username: 'bob_ross27',
  // }).then(res => console.log(res.data)).catch(err => console.log(err));

  // axios.post('http://localhost:8080/auth/register/email-check', {
  //   email: 'maxdbarton97@gmail.com',
  // }).then(res => console.log(res.data)).catch(err => console.log(err));

}).catch(err => console.log(err));

const port = process.env.PORT || '8080';
const targetFolder = 
    process.argv.includes('prod') ? 
    'dist':
    'prod';

// ============================================================================
// CONFIG
// ============================================================================

// Create server 
const app = express();

// Configure
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.set('port', 8080);
app.use(express.static(path.join(__dirname, targetFolder)));
app.use(session({
  secret: 'bobby-tarantino', 
  resave: false, 
  saveUninitialized: true
}));


// ============================================================================
// ROUTING
// ============================================================================

// get 
const register = require('./server/routes/auth/register');
const login = require('./server/routes/auth/login');
const verification = require('./server/routes/auth/verification')
const logout = require('./server/routes/auth/logout')
const getUser = require('./server/routes/get/user')
const updateUser = require('./server/routes/put/user/update')

// deal with no session..
app.get('/api/*', (req, res, next) => {
  if (!req.session.user) return res.json({ error: new Error({message: 'No User Session'}) });
  else next();
});

// define endpoints
app.use('/api/auth/register', register);
app.use('/api/auth/login', login);
app.use('/api/auth/verification', verification);
app.use('/api/auth/logout', logout);
app.use('/api/user', getUser);
app.use('/api/user/update', updateUser);


// Fallback (catch requests with no defined route)
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, `/index.html`));
});

// ============================================================================
// SERVE
// ============================================================================

 // Create HTTP server
const server = http.createServer(app);

// Listen on provided port, on all network interfaces
server.listen(port, () => console.log(`server running on port ${port}`));