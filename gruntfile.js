module.exports = function(grunt) {
    
    //non contrib tasks
    require('load-grunt-tasks')(grunt);
    
    grunt.initConfig({
        concurrent: {
            options: {
				logConcurrentOutput: true
			},
            build: {
                tasks: ['shell:serve', 'watch']
            }
        },
        shell: {
            serve: {
                command: 'node server.js'
            },
            backend: {
                command: 'nodemon server.js'
            },
        },
    });

    // contribs
    grunt.loadNpmTasks('grunt-contrib-watch');

    //tasks
    grunt.registerTask('serve', ['concurrent:build']);
    grunt.registerTask('backend', ['concurrent:build']);

};