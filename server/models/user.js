const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var userSchema = new Schema({
  uuid: { type: String, required: true, unique: true },
  email: { type: String, required: true },
  forename: { type: String, required: true },
  surnames: { type: String, required: true },
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  picture: { type: String, required: false },
  active: { type: Boolean, required: true },
  verificationCode: { type: String }
});

var User = mongoose.model('User', userSchema);

module.exports = User;