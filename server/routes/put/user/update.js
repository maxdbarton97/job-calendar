const express = require('express');
const router = express.Router();
const User = require('../../../models/user');

router.put('/', (req, res) => {
    const request = req;
    // check if username exists
    User.updateOne({uuid: request.session.user.uuid},  { $set: req.body }, (err, user) => {
        if (err) return res.json({ error: err });
        if (!user) return res.json({ user: false });
        if (user.active === false) return res.json({ user: false });
        return res.json({ user: req.body });
    });
});

module.exports = router;