const express = require('express');
const router = express.Router();
const User = require('../../models/user');
const bcrypt = require('bcrypt-nodejs');

router.post('/', (req, res) => {
  const request = req;
  // check if username exists
  User.findOne({email: request.body.email}, (err, user) => {
    if (err) return res.json({ error: err });
    if (!user) return res.json({ attempt: false });
    if (user.active === false) return res.json({ active: false });
    // check password
    const match = bcrypt.compareSync(request.body.password, user.password);
    if (match) {
      req.session.user = user;
      return res.json({ attempt: true });
    } else {
      return res.json({ attempt: false });
    }
  });
});

router.get('/check', (req, res) => {
  return res.json({ loggedIn: !!req.session.user });
});

module.exports = router;

