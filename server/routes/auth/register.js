const express = require('express');
const router = express.Router();
const User = require('../../models/user');
const uuidv4 = require('uuid/v4');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');

//
// email verification settings
//

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'nodemailertest97@gmail.com',
    pass: 'd5bef447cdPEACH'
  }
});

const mailOptions = {
  from: 'Job Calendar <nodemailertest97@gmail.com>',
  to: '', // => set during registration
  subject: 'Verify Your Email',
  text: 
`Hey (newUser),

Thank you for signing up to Job Calendar. 
      
Before you get going, please verify your email by select the following url, or copy and paste it into the address bar:
(URL)
      
See you soon!
The Job Calendar Team`
};


// when registering new users
router.post('/', (req, res, next) => {
  const newUser = new User({
    uuid: uuidv4(),
    email: req.body.email,
    forename: req.body.forename,
    surnames: req.body.surnames,
    username: req.body.username,
    password: bcrypt.hashSync(req.body.password, 10),
    active: false,
    verificationCode: uuidv4()
  });
  
  const userSearch = new Promise((rs, rj) => {
      User.findOne({email: req.body.email}, (err, doc) => {
      if (err) return rj(err);
      if (doc == null) return rs();
      if (doc.active === false) {
        return User.findOneAndRemove({email: req.body.email}, () => rs());
      }
      return rj('user already registered and verified');
    });
  });
  
  userSearch.then(() => {
    newUser.save(err => {
      if (err) return res.status(400).send(err);

      // send email verification
      mailOptions.to = newUser.email;
      mailOptions.text = mailOptions.text.replace(
       '(newUser)', 
        newUser.username
      );
      mailOptions.text = mailOptions.text.replace(
        '(URL)',
        `http://localhost:8080/verification-link/${newUser.verificationCode}`
      );
      transporter.sendMail(mailOptions, function(err, suc) {
        if (err) return res.status(400).send(err);
        return res.json(newUser);
      });
    });
  }, err => {
    res.status(400).send(err);
  })
});


// check if an email exists already
router.post('/email-check', (req, res) => {
  User.findOne({email: req.body.email}, (err, doc) => {
    if (err) return res.status(400).send(err);
    // if inactive, allow user to register again
    if (doc === null) return res.json({exists: false});
    if (doc.active === false) return res.json({exists: false});
    return res.json({exists: true});
  });
});

//check if a username exists already
router.post('/username-check', (req, res) => {
  User.findOne({username: req.body.username}, (err, doc) => {
     if (err) return res.status(400).send(err);
    // if inactive, allow user to register again
    if (doc == null) return res.json({exists: false});
    if (doc.active === false) return res.json({exists: false});
    return res.json({exists: true});
  });
});

module.exports = router;

/* usage 

  axios.post('/auth/register', {
    email: 'bobross@painting.com',
    forename: 'Bob',
    surnames: 'Ross',
    username: 'bob_ross27',
    password: 'paintme123'
  });

  */