const express = require('express');
const router = express.Router();
const User = require('../../models/user');


router.post('/check-code', (req, res) => {
  User.findOne({verificationCode: req.body.verificationCode}, (err, doc) => {
    if (err) return res.status(400).send(err);
    if (doc == null) return res.json({exists: false});
    if (doc.active == true) return res.json({verified: true});
    return res.json({verified: false});
  });
});

router.post('/verify-user', (req, res) => {
  User.findOne({verificationCode: req.body.verificationCode}, function (err, user) {
    if (err) return res.status(400).send(err);
    user.active = true;
    user.save(function (err) {
      if (err) return res.status(400).send(err);
      return res.json(user);
    });
  });
});

module.exports = router;