const express = require('express');
const router = express.Router();
const User = require('../../models/user');

router.get('/', (req, res) => {
    const request = req;
    // check if username exists
    User.findOne({uuid: request.session.user.uuid}, (err, user) => {
        if (err) return res.json({ error: err });
        if (!user) return res.json({ user: false });
        if (user.active === false) return res.json({ user: false });
        return res.json({ 
            user: {
                forename: user.forename,
                surnames: user.surnames,
                picture: user.picture,
                email: user.email
            },
     });
  });
});

module.exports = router;

