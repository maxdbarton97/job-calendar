============================================README.md==================================
# Job Calendar Codebase

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.28.3.

## Init

Run the following commands. (All text after the '-').


NVM:

- curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
- nvm install node
- nvm alias default node


Globals:

- npm install -g npm
- npm install -g @angular/cli@latest

Locals:

- npm install
- ng update --all

Error fixes:
- TypeError: Cannot read property 'thisCompilation' of undefined:
```
npm i -g @angular/cli@latest && rm -rf node_modules /rd /s /q node_modules && npm cache clear --force && npm cache verify && npm install && npm uninstall webpack && npm install --save-dev --save-exact @angular/cli@latest && npm i webpack@latest
```

This is a lot of bullshit but it works so cool beans.

## Running

To serve the app, use the following command and select the suitable link (localhost if running locally):

- grunt serve


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

Before running the tests make sure you are serving the app.

## Deploying to GitHub Pages

Run `ng github-pages:deploy` to deploy to GitHub Pages.

## Further help

To get more help on the `angular-cli` use `ng help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Helpful information

Bootstrapped using: https://scotch.io/tutorials/mean-app-with-angular-2-and-the-angular-cli#prerequisites

## Grunt commands

grunt serve - starts the default devserver from angular-cli and reads for less file changes and compiles when changes are made