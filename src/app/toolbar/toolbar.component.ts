import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service'
@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})

export class ToolbarComponent implements OnInit {
  
  constructor(private dataService:DataService) { }

  ngOnInit() {
  }
  
  openJobEntry(): void {
    const today = new Date();
    this.dataService.openJobEntry(
      new Date(
        today.getFullYear(),
        today.getMonth(),
        today.getDate(),
      ), null);
  }
}
