import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-profile-notifications',
  templateUrl: './profile-notifications.component.html',
  styleUrls: ['./profile-notifications.component.css']
})
export class ProfileNotificationsComponent implements OnInit {

  constructor() { }

  notificationsForm: FormGroup = new FormGroup({
    'daily': new FormControl(true),
    'weekly': new FormControl(true),
    'monthly': new FormControl(true),
    'overdue': new FormControl(true),
    //
    'accepted': new FormControl(true),
    'assigned': new FormControl(true),
    'unassigned': new FormControl(true),
    'completed': new FormControl(true),
    'edited': new FormControl(true),
    'archived': new FormControl(true),

  });

  ngOnInit() {
  }

}
