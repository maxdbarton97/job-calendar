import { Component, OnInit } from '@angular/core';
import {Router, NavigationEnd } from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private router: Router) { }

  selected: string;

  ngOnInit() {

    this.selected = this.router.url;
    // read the route data and set the selected member
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.selected = event.url;
      }
    });

  }
}
