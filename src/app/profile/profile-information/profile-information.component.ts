import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from 'app/services/data.service';

@Component({
  selector: 'app-profile-information',
  templateUrl: './profile-information.component.html',
  styleUrls: ['./profile-information.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ProfileInformationComponent implements OnInit {
  croppedPicture: string;
  imageForCropping: string;
  imageHover: boolean;
  user: any = {};
  picture: string = '';
  formChanges: boolean = false;
  userChangeSub: any;
  informationForm: FormGroup = new FormGroup({
    'forename': new FormControl('', [Validators.required]),
    'surnames': new FormControl('', [Validators.required]),
    'email': new FormControl('', [Validators.required, Validators.email]),
  });

  constructor(private dataService: DataService) {
    this.userChangeSub = this.dataService.userChange.subscribe(user => {
      this.user = user;
      this.setFormData();
      this.picture = user.picture;
      this.formChanges = false;
    });
    this.dataService.getProfile();
  };

  ngOnInit() {};
  ngOnDestroy() {
    this.userChangeSub.unsubscribe();
  }


  @ViewChild('pictureInput') private pictureInput: ElementRef;

  editPicture() {
    this.pictureInput.nativeElement.click();
  }

  onPictureChange(event) {
    // load the cropper
    const file = event.target.files[0];
    getBase64(file).then(base64 => {
      this.imageForCropping = `data:image/png;base64,${base64}`;
    })
  }

  cropImage(event) {
    compressImage(event.base64, 100, 100).then(compressed => {
      this.croppedPicture = compressed.toString();
    })
  }

  confirmCrop() {
    this.imageForCropping = '';
    this.formChanges = true;
    this.picture = this.croppedPicture;
  }

  cancelCrop() {
    this.imageForCropping = '';
    this.formChanges = false;
  }

  removePicture() {
    this.picture = '';
    this.formChanges = true;
  }

  onInputChange(): void {
    this.formChanges = true;
    if (this.informationForm.controls['forename'].value != this.user.forename) return;
    if (this.informationForm.controls['surnames'].value != this.user.surnames) return;
    if (this.informationForm.controls['email'].value != this.user.email) return;
    this.formChanges = false;
  }

  setFormData(): void {
    this.informationForm.controls['forename'].setValue(this.user.forename);
    this.informationForm.controls['surnames'].setValue(this.user.surnames);
    this.informationForm.controls['email'].setValue(this.user.email);
    this.picture = this.user.picture;
    this.formChanges = false;
  }

  submit(): void {
    const data = {
      forename: this.informationForm.controls['forename'].value,
      surnames: this.informationForm.controls['surnames'].value,
      picture: this.picture,
      email: this.informationForm.controls['email'].value
    }
    this.dataService.updateProfile(data);
  }
}

//
// helpers
//

function compressImage(src, newX, newY) {
  return new Promise((res, rej) => {
    const img = new Image();
    img.src = src;
    img.onload = () => {
      const elem = document.createElement('canvas');
      elem.width = newX;
      elem.height = newY;
      const ctx = elem.getContext('2d');
      ctx.drawImage(img, 0, 0, newX, newY);
      const data = ctx.canvas.toDataURL();
      res(data);
    }
    img.onerror = error => rej(error);
  })
}

function getBase64(image) {
  return new Promise((res, rej) => {
    const reader = new FileReader();
    reader.readAsBinaryString(image);
    reader.onload = e => res(btoa(e.target['result']));
    reader.onerror = err => rej(err);
  })
}

