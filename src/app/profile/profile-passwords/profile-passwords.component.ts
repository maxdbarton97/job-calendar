import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile-passwords',
  templateUrl: './profile-passwords.component.html',
  styleUrls: ['./profile-passwords.component.css']
})
export class ProfilePasswordsComponent implements OnInit {

  constructor() { }

  passwordsForm: FormGroup = new FormGroup({
    'old': new FormControl('', [Validators.required]),
    'new': new FormControl('', [Validators.required]),
    'confirmNew': new FormControl('', [Validators.required, Validators.email])
  });

  ngOnInit() {
  }

}
