import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-profile-preferences',
  templateUrl: './profile-preferences.component.html',
  styleUrls: ['./profile-preferences.component.css']
})
export class ProfilePreferencesComponent implements OnInit {

  constructor() { }

  preferencesForm: FormGroup = new FormGroup({
    'darkTheme': new FormControl(false),
    'emailUpdates': new FormControl(true),
    'timeFormat': new FormControl('uk'),
    'language': new FormControl('english'),
  });
  ngOnInit() {
  }

}
