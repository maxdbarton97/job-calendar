import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/services/data.service';
import { Job } from 'app/mods/job';

@Component({
  selector: 'app-job-view',
  templateUrl: './job-view.component.html',
  styleUrls: ['./job-view.component.css']
})
export class JobViewComponent implements OnInit {

  constructor(private dataService: DataService) { }

  isOpen: boolean;
  job;
  users: Object[];
  userUuid: string;
  user: Object;
  meInJobUsers: Boolean;

  objectKeys = Object.keys;

  ngOnInit() {
    if (this.dataService.session) this.dataService.getProfile();
    this.dataService.userChange.subscribe(user => {
      this.userUuid = user.uuid
    })

    this.users = this.dataService.getUsers(this.dataService.selectedTeamUuid);
    this.getUser();
    this.dataService.usersChange.subscribe(value => { 
      if (value.teamUuid = this.dataService.selectedTeamUuid) {
        this.users = value.users;
        this.getUser();
      }
    })
    
    this.dataService.jobViewChange.subscribe(value => {
      this.isOpen = !!value;
      this.job = value;
      if (this.isOpen) {
        this.meInJobUsers = false;
        this.job.users.forEach(user => {
          if (user['uuid'] == this.user['uuid']) {
            this.meInJobUsers = true;
          }
        })
      }
    });
  }

  getUser(): void {
    this.users.forEach(user => {
      if (user['uuid'] == this.userUuid) {
        this.user = user;
      }
    });
  }

  close() {
    this.dataService.closeJobView();
  }

  openColor(color) {
    window.open(`https://www.color-hex.com/color/${color.slice(1, color.length)}`)
  }

  edit() {
    this.dataService.openJobEntry(this.job.date, this.job);
    this.dataService.closeJobView();
  }

  archive() {
    this.dataService.archive(this.job);
    this.dataService.closeJobView();
  }

  complete() {
    this.dataService.complete(this.job);
    this.dataService.closeJobView();
  }

  reopen() {
    this.dataService.reopen(this.job);
    this.dataService.closeJobView();
  }
}