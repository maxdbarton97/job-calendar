// angular imports 
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MobxAngularModule } from 'mobx-angular';
import { RouterModule, Routes } from '@angular/router';

// material
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatFormFieldModule, MatInputModule,
         MAT_DATE_LOCALE} from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ImageCropperModule } from 'ngx-image-cropper';


// components 
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { JobEntryComponent } from './job-entry/job-entry.component';
import { JobViewComponent } from './job-view/job-view.component';
import { UserCardComponent } from './user-card/user-card.component';
import { HomeComponent } from './home/home.component';
import { SpinnerComponent } from './home/spinner/spinner.component';
import { CalendarTileComponent } from './home/calendar/calendar-tile/calendar-tile.component';
import { CalendarComponent } from './home/calendar/calendar.component';
import { FiltersComponent } from './home/filters/filters.component';
import { DatepickerComponent } from './home/datepicker/datepicker.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileInformationComponent } from './profile/profile-information/profile-information.component';
import { ProfilePasswordsComponent } from './profile/profile-passwords/profile-passwords.component';
import { ProfileNotificationsComponent } from './profile/profile-notifications/profile-notifications.component';
import { ProfilePreferencesComponent } from './profile/profile-preferences/profile-preferences.component';
import { ProfileDeactivateComponent } from './profile/profile-deactivate/profile-deactivate.component';
import { CalendarListComponent } from './components/calendar-list/calendar-list.component';
import { TeamComponent } from './team/team.component';
import { TeamDetailsComponent } from './team/team-details/team-details.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { VerifyAccountComponent } from './verify-account/verify-account.component';
import { VerificationLinkComponent } from './verification-link/verification-link.component';
import { DataService } from './services/data.service';
import { BackgroundComponent } from './components/background/background.component';
import { PersonalComponent } from './routes/personal/personal.component';
import { MemberDetailsComponent } from './team/member-details/member-details.component';

const appRoutes: Routes = [
  {
    path: 'verification-link/:verificationCode',
    component: VerificationLinkComponent,
    data: {}
  },
  {
    path: 'verify-account',
    component: VerifyAccountComponent,
    data: { sessionless: true }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: { sessionless: true }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { sessionless: true }
  },
  {
    path: 'home',
    component: HomeComponent,
    data: { }
  },
  {
    path: 'team/:teamUuid',
    component: TeamComponent,
    children: [
      {
        path: '',
        component: TeamDetailsComponent,
        data: {overview: true}
      }, 
      {
        path: 'users/:userUuid',
        component: MemberDetailsComponent
      },
    ]
  },
  {
    path: 'personal',
    component: PersonalComponent
  },
  {
    path: 'profile',
    component: ProfileComponent,
    data: {},
    children: [
      {
        path: '',
        redirectTo: 'information',
        pathMatch: 'full'
      }, 
      {
        path: 'information',
        component: ProfileInformationComponent,
      }, 
      {
        path: 'passwords',
        component: ProfilePasswordsComponent,
      }, 
      {
        path: 'notifications',
        component: ProfileNotificationsComponent,
      },
      {
        path: 'preferences',
        component: ProfilePreferencesComponent,
      }, 
      {
        path: 'deactivate',
        component: ProfileDeactivateComponent,
      }, 
    ]
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

import { HttpClient } from '@angular/common/http';

export function checkSession(http: HttpClient, dataService: DataService): 
() => Promise<any> {
  return (): Promise<any> => {
    return new Promise((resolve, reject) => {
      http.get('/api/auth/login/check').subscribe((response: {loggedIn: boolean}) => {
        if(response.loggedIn) {
          dataService.session = true;
        }
        resolve();
      })
    });
  };
}

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    DatepickerComponent,
    SpinnerComponent,
    FiltersComponent,
    CalendarComponent,
    CalendarTileComponent,
    JobEntryComponent,
    JobViewComponent,
    UserCardComponent,
    HomeComponent,
    FiltersComponent,
    ProfileComponent,
    ProfileInformationComponent,
    ProfilePasswordsComponent,
    ProfileNotificationsComponent,
    ProfilePreferencesComponent,
    ProfileDeactivateComponent,
    CalendarListComponent,
    TeamComponent,
    MemberDetailsComponent,
    TeamDetailsComponent,
    LoginComponent,
    RegisterComponent,
    VerifyAccountComponent,
    VerificationLinkComponent,
    BackgroundComponent,
    PersonalComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    MatToolbarModule,
    FlexLayoutModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatTableModule,
    MatTooltipModule,
    MatDividerModule,
    MobxAngularModule,
    MatButtonToggleModule,
    MatMenuModule,
    MatCardModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    ImageCropperModule
  ],
  providers: [
    {
      provide: MAT_DATE_LOCALE,
      useValue: 'en-GB' 
    },
    {
      provide: APP_INITIALIZER,
      useFactory: checkSession,
      deps: [HttpClient, DataService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }