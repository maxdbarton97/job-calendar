import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, AbstractControl, FormGroup, FormBuilder } from '@angular/forms';

import { DatePacket } from 'app/mods/date-packet';
import { DateService } from 'app/services/date.service';


@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.css']
})

export class DatepickerComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private dateService: DateService,
    private el: ElementRef,
    private cdr: ChangeDetectorRef
  ) { };
  
  ngOnInit() {
    this.buttonDates = this.dateService.getButtonDates();
    this.selectedDate = this.dateService.selectedDate;
    // init validation for input 
    this.myForm.get('date').valueChanges.subscribe(val => this.inputDateCheck());
    // when the month is manually changed
    this.dateService.change.subscribe(value => this.selectedDate = value);
  };
  
  @ViewChild('datepicker') datepicker: ElementRef;
  
  onResize(event) { 
    this.fitButtons();
  };

  buttonWidths = {};
  ngAfterViewInit() {
    this.displayedButtons = this.dateService.getButtonDates();
    setTimeout(() => this.fitButtons(), 1);
    this.cdr.detectChanges(); // fixes change detection bug
    
    // take width of each datepicker button and map into array so it can be used
    // to display the perfect amount of buttons on screen 
    for (const child of this.datepicker.nativeElement.children) {
      if (child.classList.contains('date-button')) {
        this.buttonWidths[child.dataset.month] = child.clientWidth;
      }
    }
  };

  //
  // members
  //
  
  buttonDates: DatePacket[];
  months: DatePacket[];
  selectedDate: DatePacket;
  dateFocused: boolean;
  dateVal: string = '5';
  currentDateVal: string = '';
  inputDate: boolean;
  displayedButtons: DatePacket[];
  // BehaviorSubject fixes -> https://stackoverflow.com/questions/34364880/expression-has-changed-after-it-was-checked

  // validator function
  customDateValidator(control: AbstractControl): { [key: string]: boolean } | null {
    let valid = true;
    const obj = {'invalidDate': true};
    // for each character, check if it's a number except the '/';
    for (let c = 0; c < control.value.length; c++) {
      if (isNaN(control.value[c]) && c != 2) valid = false;
    }
    if (!valid) return obj;
    // if the month is valid
    if (Number(control.value[0]) > 1 || (Number(control.value[0]) == 1 && Number(control.value[1]) > 2)) {
      return obj;
    }
    if (control.value.length < 7 && this.dateFocused == false) return obj;
    if (control.value.length > 7) return obj;
    return null;
  };
  
  dateControl: FormControl = new FormControl('', {validators: [this.customDateValidator.bind(this)]});
  myForm: FormGroup = this.fb.group({'date': this.dateControl});
  
  //
  // functions
  //

  onDateBlur(): void {
    this.dateFocused = false;
  };
  
    onDateFocus(): void {
    this.dateFocused = true;
  };
  
  selectDate(date): void {
    this.inputDate = false;
    this.dateService.selectedDate = date;
    this.selectedDate = date;
  };
  
  inputDateCheck(): void {
    const increase = (this.currentDateVal.length < this.dateControl.value.length) ? true : false;
    if (this.dateControl.value.length == 2 && increase) {
      this.dateControl.setValue(this.dateControl.value + '/');
    }
    if (this.dateControl.value.length == 2 && !increase) this.dateControl.setValue(this.dateControl.value[0]);
    this.currentDateVal = this.dateControl.value;
  };
  
  viewInput(): void {
    // create date object from input
    const inputDate = new Date(
      this.dateControl.value.slice(3, 7),
      this.dateControl.value.slice(0, 2)-1, 
      1
    );
    this.selectedDate = this.dateService.createDatePacket(inputDate);
    this.dateService.selectedDate = this.selectedDate;
    this.inputDate = true;
  };
  
  fitButtons(): void {
    const windowWidth = window.innerWidth;
    const datepickerWidth = this.datepicker.nativeElement.scrollWidth;
    let flex;
    for (const child of this.datepicker.nativeElement.children) {
      if (child.classList.contains('flex')) {
        flex = child.clientWidth;
      }
    }
    
    if (datepickerWidth > windowWidth) {
      // calculate the difference and see what needs to be removed
      let diff = (datepickerWidth  + 20) - windowWidth;
      let count = 0;
      while (diff > 50) {
        diff -= this.buttonWidths[Object.keys(this.buttonWidths)[count]];
        count += 1;
      }
      this.displayedButtons = this.displayedButtons.slice(0, this.displayedButtons.length - count - 1);
    }
    
    // append back on if any remaining space
    let buttonsToAppend = Object.keys(this.buttonDates).filter(x => Object.keys(this.displayedButtons).indexOf(x) < 0);
    
    while (buttonsToAppend.length > 0 && flex > this.buttonWidths[this.buttonDates[Number(buttonsToAppend[0])].text]) {
      flex -= this.buttonWidths[this.buttonDates[Number(buttonsToAppend[0])].text];
      this.displayedButtons.push(this.buttonDates[Number(buttonsToAppend[0])]);
      buttonsToAppend.shift();
    }
  }
}