"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var DatepickerComponent = /** @class */ (function () {
    function DatepickerComponent(fb, dateService, el) {
        this.fb = fb;
        this.dateService = dateService;
        this.el = el;
        this.buttonWidths = {};
        this.dateVal = '5';
        this.currentDateVal = '';
        this.dateControl = new forms_1.FormControl('', { validators: [this.customDateValidator.bind(this)] });
        this.myForm = this.fb.group({ 'date': this.dateControl });
    }
    ;
    DatepickerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.buttonDates = this.dateService.getButtonDates();
        this.displayedButtons = this.dateService.getButtonDates();
        this.selectedDate = this.dateService.selectedDate;
        // init validation for input 
        this.myForm.get('date').valueChanges.subscribe(function (val) { return _this.inputDateCheck(); });
        // when the month is manually changed
        this.dateService.change.subscribe(function (value) { return _this.selectedDate = value; });
    };
    ;
    DatepickerComponent.prototype.onResize = function (event) {
        this.fitButtons();
    };
    ;
    DatepickerComponent.prototype.ngAfterViewInit = function () {
        // take width of each datepicker button and map into array so it can be used
        // to display the perfect amount of buttons on screen 
        for (var _i = 0, _a = this.datepicker.nativeElement.children; _i < _a.length; _i++) {
            var child = _a[_i];
            if (child.classList.contains('date-button')) {
                this.buttonWidths[child.dataset.month] = child.clientWidth;
            }
        }
    };
    ;
    // validator function
    DatepickerComponent.prototype.customDateValidator = function (control) {
        var valid = true;
        var obj = { 'invalidDate': true };
        // for each character, check if it's a number except the '/';
        for (var c = 0; c < control.value.length; c++) {
            if (isNaN(control.value[c]) && c != 2)
                valid = false;
        }
        if (!valid)
            return obj;
        // if the month is valid
        if (Number(control.value[0]) > 1 || (Number(control.value[0]) == 1 && Number(control.value[1]) > 2)) {
            return obj;
        }
        if (control.value.length < 7 && this.dateFocused == false)
            return obj;
        if (control.value.length > 7)
            return obj;
        return null;
    };
    ;
    //
    // functions
    //
    DatepickerComponent.prototype.onDateBlur = function () {
        this.dateFocused = false;
    };
    ;
    DatepickerComponent.prototype.onDateFocus = function () {
        this.dateFocused = true;
    };
    ;
    DatepickerComponent.prototype.selectDate = function (date) {
        this.inputDate = false;
        this.dateService.setSelectedDate(date);
        this.selectedDate = date;
    };
    ;
    DatepickerComponent.prototype.inputDateCheck = function () {
        var increase = (this.currentDateVal.length < this.dateControl.value.length) ? true : false;
        if (this.dateControl.value.length == 2 && increase) {
            this.dateControl.setValue(this.dateControl.value + '/');
        }
        if (this.dateControl.value.length == 2 && !increase)
            this.dateControl.setValue(this.dateControl.value[0]);
        this.currentDateVal = this.dateControl.value;
    };
    ;
    DatepickerComponent.prototype.viewInput = function () {
        // create date object from input
        var inputDate = new Date(this.dateControl.value.slice(3, 7), this.dateControl.value.slice(0, 2) - 1, 1);
        this.selectedDate = this.dateService.createDatePacket(inputDate);
        this.dateService.setSelectedDate(this.selectedDate);
        this.inputDate = true;
    };
    ;
    DatepickerComponent.prototype.fitButtons = function () {
        var _this = this;
        var windowWidth = window.innerWidth;
        var datepickerWidth = this.datepicker.nativeElement.scrollWidth;
        if (datepickerWidth > windowWidth) {
            // calculate the difference and see what needs to be removed
            var diff = datepickerWidth - windowWidth;
            var count = 0;
            while (diff > 20) { // 20 is body margin
                diff -= this.buttonWidths[Object.keys(this.buttonWidths)[count]];
                count += 1;
            }
            this.displayedButtons = this.displayedButtons.slice(0, this.displayedButtons.length - count - 1);
        }
        // get width of the flex (available space) 
        var flex;
        for (var _i = 0, _a = this.datepicker.nativeElement.children; _i < _a.length; _i++) {
            var child = _a[_i];
            if (child.classList.contains('flex')) {
                flex = child.clientWidth;
            }
        }
        if (datepickerWidth < windowWidth) {
            diff = windowWidth - datepickerWidth;
            var buttonsToAppend = Object.keys(this.buttonDates).filter(function (x) { return Object.keys(_this.displayedButtons).indexOf(x) < 0; });
            while (diff > this.buttonWidths[this.buttonDates[Number(buttonsToAppend[0])].text]) {
                diff -= this.buttonWidths[this.buttonDates[Number(buttonsToAppend[0])].text];
                this.displayedButtons.push(this.buttonDatesNumber(buttonsToAppend[0]));
                buttonsToAppend.shift();
            }
        }
    };
    __decorate([
        core_1.ViewChild('datepicker')
    ], DatepickerComponent.prototype, "datepicker");
    DatepickerComponent = __decorate([
        core_1.Component({
            selector: 'app-datepicker',
            templateUrl: './datepicker.component.html',
            styleUrls: ['./datepicker.component.css']
        })
    ], DatepickerComponent);
    return DatepickerComponent;
}());
exports.DatepickerComponent = DatepickerComponent;
