import { Component, OnInit, Input } from '@angular/core';
import { DateService } from 'app/services/date.service';
import { Job } from 'app/mods/job';
import { DataService } from 'app/services/data.service';
import { DatePacket } from 'app/mods/date-packet';


@Component({
  selector: 'app-calendar-tile',
  templateUrl: './calendar-tile.component.html',
  styleUrls: ['./calendar-tile.component.css']
})
export class CalendarTileComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private dateService: DateService,
  ) {}

  params: object;
  jobs = []
  selectedDate: DatePacket;
  overdue: Boolean;
  incompleteJobTicker: Boolean;
  gridPosition: number;
  createButton: boolean;
  displayButtonTimeout: any;
  filteredJobs = [];
  filters = {
    users: [],
    places: [],
    tags: [],
    status: [],
    activity: []
  }

  @Input() data: any;
  today: boolean = false;

  ngOnInit() {
    this.selectedDate = this.dateService.selectedDate;

    if (new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toString() ==
    new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth(), this.data.date).toString()) this.today = true;

    this.filters = this.dataService.filters;
    this.jobs = this.dataService.getJobs(
      new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth(), this.data.date),
      null
    );
    this.filteredJobs = this.filterJobs();

    //==========
    // listeners
    //==========

    this.dateService.change.subscribe(value => {
      this.selectedDate = value;
    });

    this.dataService.jobChange.subscribe(() => {
      this.jobs = this.dataService.getJobs(
        new Date(
          this.selectedDate.date.getFullYear(),
          this.selectedDate.date.getMonth(),
          this.data.date
        ), null
      );
      this.filteredJobs = this.filterJobs()
    })

    this.dataService.filtersChange.subscribe(filters => {
      this.filters = filters;
      this.filteredJobs = this.filterJobs();
    });

    this.dataService.selectedTeamChange.subscribe(() => {
      this.jobs = this.dataService.getJobs(
        new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth(), this.data.date),
        null
      );
      this.filteredJobs = this.filterJobs()
    });


    // itterate through jobs and decide if overdue!
    for (let job of this.jobs) {
      if (new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()) >
        new Date(job.date.getFullYear(), job.date.getMonth(), job.date.getDate()) && !job.completed) {
          job.overdue = true;
      }
    }

    setInterval(() => {
      this.incompleteJobTicker = !this.incompleteJobTicker
    }, 1000);
  };

  // listen to parent changes..
  ngOnChanges(params) {
    if (!this.selectedDate) this.selectedDate = this.dateService.selectedDate;
    if (params.data) this.data = params.data.currentValue;
    this.jobs = this.dataService.getJobs(
      new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth(), this.data.date,
      null
    ));
    this.filteredJobs = this.filterJobs();
  }



  filterJobs() {
    return this.jobs.filter(job => {
      if (this.filters['status'].length == 0) return false;
      if (this.filters['status'].length < 2 && (this.filters['status'].includes('completed') && !job.completed)) return false;
      if (this.filters['status'].length < 2 && (this.filters['status'].includes('open') && job.completed)) return false;
      if (this.filters['activity'].length == 0) return false;
      if (this.filters['activity'].length < 2 && (this.filters['activity'].includes('archived') && !job.archived)) return false;
      if (this.filters['activity'].length < 2 && (this.filters['activity'].includes('active') && job.archived)) return false;
      if (!this.filters.places.map(place => { return place.uuid }).includes(job.place.uuid)) return false;
      for (let x = 0; x < job.users.length; x++) {
        if (!this.filters['users'].map(user => { return user.uuid }).includes(job.users[x].uuid)) return false;
      }
      return true;
    });
  };

  displayCreateButton(): void {
    this.displayButtonTimeout = window.setTimeout(() => {
      this.createButton = true
    }, 300);
  }

  hideCreateButton(): void {
    window.clearTimeout(this.displayButtonTimeout);
    this.createButton = false
  };

  createJob(date): void {
    const month = this.dateService.selectedDate;
    this.dataService.openJobEntry(
      new Date(
        month.date.getFullYear(),
        month.date.getMonth(),
        date
      ), 
      false
    );
  }
  
  openJobEntry(job): void {
    this.dataService.openJobEntry(job.date, job);
  }

  openJobView(job): void {
    this.dataService.openJobView(job);
  }
}
