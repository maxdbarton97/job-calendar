import { Component, OnInit } from '@angular/core';

import { DatePacket } from 'app/mods/date-packet';
import { DateService } from 'app/services/date.service';
import { DataService } from 'app/services/data.service';


@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {


  constructor(
    private dateService: DateService,
    private dataService: DataService,
  ) { };
  
  ngOnInit() {
    this.selectedDate = this.dateService.selectedDate
    this.initGridData();
    
    this.dateService.change.subscribe(value => {
      this.selectedDate = value
      this.initGridData();
    });

  };
  
  gridData: Object = {};
  jobs: Object = {};
  calandarHeight: number = window.innerHeight - 215;
  filters: object;
  
  onResize(): void { 
    this.calandarHeight = window.innerHeight - 215; // 215 = height above calendar...
  };
  
  monthMap = {
    0: 'January ',
    1: 'February ',
    2: 'March ',
    4: 'May ',
    3: 'April ',
    5: 'June ',
    6: 'July ',
    7: 'August ',
    8: 'September ',
    9: 'October ',
    10: 'November ',
    11: 'December ',
    12: 'January ' // when overflown
  };
  
  columns: number[] = [1, 2, 3, 4, 5, 6, 7];
  numberOfDays: number;
  firstDay: number;
  firstGridPosition: number;
  lastGridPosition: number;
  prevMonthNumberOfDays: number;
  selectedDate: DatePacket;
  rows: number[];
  rowNumber: number;


  initGridData(): void {
    this.numberOfDays = new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth() + 1, 0).getDate();
    this.firstDay = new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth(), 1).getDay();
    
    //use ternary to decide row numbers
    this.rowNumber = 
      this.numberOfDays == 31 && this.firstDay <= 4 ? 5 :
      this.numberOfDays == 31 && this.firstDay > 4 ? 6 :
      this.numberOfDays == 30 && this.firstDay <= 5 ? 5 :
      this.numberOfDays == 30 && this.firstDay > 5 ? 6 :
      this.numberOfDays == 29 && this.firstDay <= 6 ? 5 :
      this.numberOfDays == 29 && this.firstDay > 6 ? 6 :
      this.numberOfDays == 28 && this.firstDay >= 1 ? 5 : 4;
  
    // map the row number into an array of accending numbers for use of grid tiles
    this.rows = Array.from(new Array(this.rowNumber),(val,index)=>index+1);
  
    //get the grid position of the 1st and then the last date (e.g 30th)
    this.firstGridPosition = new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth(), 1).getDay() + 1;
    this.lastGridPosition = new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth() + 1, 0).getDate() + this.firstGridPosition - 1;
    this.prevMonthNumberOfDays = new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth(), 0).getDate();
  
    let y = 1;
    // init grid data and start by mapping date on each tile
    for (let x = 1; x <= this.columns.length * this.rows.length; x++) {
      // previous month
      if (x < this.firstGridPosition) {
        this.gridData[x] = {
          date: (1-Math.abs(this.firstGridPosition-x)).toString(),
          text: this.prevMonthNumberOfDays - this.firstGridPosition + x + 1
        };
      }
      // 1st month
      if (x == this.firstGridPosition) {
        this.gridData[x] = {
          date: y.toString(),
          text: `${y} ${this.monthMap[this.selectedDate.date.getMonth()]}`
        };
        y++;
      }
      // all month days up until last
      if (x > this.firstGridPosition && x <= this.lastGridPosition) {
        this.gridData[x] = {
          date: y.toString(),
          text: y
        };
        y++
      }
      // next month 1st day
      if (x == this.lastGridPosition + 1) {
        this.gridData[x] = {
          date: y.toString(),
          text: `${x - this.numberOfDays - this.firstGridPosition + 1} ${this.monthMap[this.selectedDate.date.getMonth()+1]}`
        }
        y++
      }
      //next month second onwards  
      if (x > this.lastGridPosition + 1) {
        this.gridData[x] = {
          date: y.toString(),
          text: x - this.numberOfDays - this.firstGridPosition + 1
        };   
        y++
      }
    };
  }
}
