"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var CalendarComponent = /** @class */ (function () {
    function CalendarComponent(dateService, dataService) {
        this.dateService = dateService;
        this.dataService = dataService;
        this.gridData = {};
        this.jobs = {};
        this.monthMap = {
            0: 'January ',
            1: 'February ',
            2: 'March ',
            4: 'May ',
            3: 'April ',
            5: 'June ',
            6: 'July ',
            7: 'August ',
            8: 'September ',
            9: 'October ',
            10: 'November ',
            11: 'December ',
            12: 'January ' // when overflown
        };
        this.columns = [1, 2, 3, 4, 5, 6, 7];
    }
    ;
    CalendarComponent.prototype.ngOnInit = function () {
        var _this = this;
        // init the data
        this.selectedDate = this.dateService.selectedDate;
        this.jobs = this.dataService.getJobs(new Date(2018, 9));
        this.initGridData();
        // when the month is manually changed
        this.dateService.change.subscribe(function (value) {
            _this.selectedDate = value;
            _this.jobs = _this.dataService.getJobs(_this.selectedDate.date);
            _this.initGridData();
        });
    };
    ;
    CalendarComponent.prototype.initGridData = function () {
        this.numberOfDays = new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth() + 1, 0).getDate();
        this.firstDay = new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth(), 1).getDay();
        //use ternary to decide row numbers
        this.rowNumber =
            this.numberOfDays == 31 && this.firstDay <= 4 ? 5 :
                this.numberOfDays == 31 && this.firstDay > 4 ? 6 :
                    this.numberOfDays == 30 && this.firstDay <= 5 ? 5 :
                        this.numberOfDays == 30 && this.firstDay > 5 ? 6 :
                            this.numberOfDays == 29 && this.firstDay <= 6 ? 5 :
                                this.numberOfDays == 29 && this.firstDay > 6 ? 6 :
                                    this.numberOfDays == 28 && this.firstDay >= 1 ? 5 : 4;
        // map the row number into an array of accending numbers for use of grid tiles
        this.rows = Array.from(new Array(this.rowNumber), function (val, index) { return index + 1; });
        //get the grid position of the 1st and then the last date (e.g 30th)
        this.firstGridPosition = new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth(), 1).getDay() + 1;
        this.lastGridPosition = new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth() + 1, 0).getDate() + this.firstGridPosition - 1;
        this.prevMonthNumberOfDays = new Date(this.selectedDate.date.getFullYear(), this.selectedDate.date.getMonth(), 0).getDate();
        var y = 1;
        // init grid data and start by mapping date on each tile
        for (var x = 1; x <= this.columns.length * this.rows.length; x++) {
            // previous month
            if (x < this.firstGridPosition) {
                this.gridData[x] = {
                    date: (1 - Math.abs(this.firstGridPosition - x)).toString(),
                    text: this.prevMonthNumberOfDays - this.firstGridPosition + x + 1
                };
            }
            // 1st month
            if (x == this.firstGridPosition) {
                this.gridData[x] = {
                    date: y.toString(),
                    text: y + " " + this.monthMap[this.selectedDate.date.getMonth()]
                };
                y++;
            }
            // all month days up until last
            if (x > this.firstGridPosition && x <= this.lastGridPosition) {
                this.gridData[x] = {
                    date: y.toString(),
                    text: y
                };
                y++;
            }
            // next month 1st day
            if (x == this.lastGridPosition + 1) {
                this.gridData[x] = {
                    date: this.lastGridPosition.toString(),
                    text: x - this.numberOfDays - this.firstGridPosition + 1 + " " + this.monthMap[this.selectedDate.date.getMonth() + 1]
                };
            }
            //next month second onwards  
            if (x > this.lastGridPosition + 1) {
                this.gridData[x] = {
                    date: (x - this.numberOfDays - this.firstGridPosition + 1).toString(),
                    text: x - this.numberOfDays - this.firstGridPosition + 1
                };
            }
        }
        ;
    };
    CalendarComponent = __decorate([
        core_1.Component({
            selector: 'app-calendar',
            templateUrl: './calendar.component.html',
            styleUrls: ['./calendar.component.css']
        })
    ], CalendarComponent);
    return CalendarComponent;
}());
exports.CalendarComponent = CalendarComponent;
