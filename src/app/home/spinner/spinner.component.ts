import { Component, OnInit } from '@angular/core';

import { DateService } from 'app/services/date.service';
import { DatePacket } from 'app/mods/date-packet';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent implements OnInit {

  constructor(private dateService: DateService) { };

  selectedDate: DatePacket;
  
  ngOnInit() {
    this.selectedDate = this.dateService.selectedDate
    // when the month is manually changed
    this.dateService.change.subscribe(value => this.selectedDate = value);
  };
  
  monthMap = {
    1: 'February ',
    0: 'January ',
    2: 'March ',
    4: 'May ',
    3: 'April ',
    5: 'June ',
    6: 'July ',
    7: 'August ',
    8: 'September ',
    9: 'October ',
    10: 'November ',
    11: 'December ',
  };
  
  backwards(): void {
    // get date and add one
    const newMonth = (this.selectedDate.date.getMonth() == 0) ? 11 : this.selectedDate.date.getMonth() - 1;
    const newYear = (newMonth == 11) ? this.selectedDate.date.getFullYear() - 1 : this.selectedDate.date.getFullYear();
    const newDatePacket = this.dateService.createDatePacket(new Date(newYear, newMonth, 1));
    this.dateService.selectedDate = newDatePacket;
  }
  
  forwards(): void {
    // get date and add one
    const newMonth = (this.selectedDate.date.getMonth() == 11) ? 0 : this.selectedDate.date.getMonth() + 1;
    const newYear = (newMonth == 0) ? this.selectedDate.date.getFullYear() + 1 : this.selectedDate.date.getFullYear();
    const newDatePacket = this.dateService.createDatePacket(new Date(newYear, newMonth, 1));
    this.dateService.selectedDate = newDatePacket
  }

}
