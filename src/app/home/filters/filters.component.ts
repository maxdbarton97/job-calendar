import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/services/data.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})

export class FiltersComponent implements OnInit {
  constructor(private dataService: DataService) {};

  // the full lists
  teamUuid: string;
  users: string[];
  places: string[];
  tags: object[];
  status: string[];
  activity: string[];

  // the current values used in the filters
  usersList: string[];
  placesList: string[];
  tagsList: Object[];
  statusList: string[];
  activityList: string[];

  ngOnInit() {
    this.teamUuid = this.dataService.selectedTeamUuid;
    this.usersList = this.dataService.getUsers();
    this.placesList = this.dataService.getPlaces();
    this.tagsList = this.dataService.getTags();
    this.statusList = ['completed', 'open'];
    this.activityList = ['active', 'archived'];
    
    setTimeout(() => this.setDefaults());

    this.dataService.usersChange.subscribe(value => {
      this.usersList = value.users;
      this.users = this.usersList.slice();
      this.updateFilters('users', this.users);
    });

    this.dataService.placesChange.subscribe(value => {
      this.placesList = value.places;
      this.places = this.placesList.slice();
      this.updateFilters('places', this.placesList);
    });

    this.dataService.tagsChange.subscribe(value => {
      this.tagsList = value;
      this.tags = this.tagsList.slice();
      this.updateFilters('tags', this.tagsList.slice);
    });

    this.dataService.selectedTeamChange.subscribe(value => {
      this.teamUuid = value;
      this.setDefaults();
    });
  };

  setDefaults() {
    this.users = this.usersList.slice();
    this.updateFilters('users', this.users);

    this.places = this.placesList.slice();
    this.updateFilters('places', this.places);

    this.tags = this.tagsList.slice();
    this.updateFilters('tags', this.tags);

    this.status = this.statusList.slice();
    this.updateFilters('status', this.status);

    this.activity = this.activityList.slice(0, -1);
    this.updateFilters('activity', this.activity);
  }

  selectAll(filter, filterList) {
    this[filter] = this[filterList].slice();
    this.updateFilters(filter, this[filter])
  }

  removeAll(filter) {
    this[filter] = []
    this.updateFilters(filter, this[filter]);
  };

  updateFilters(filterType, filter) {
    this.dataService.updateFilter(filterType, filter)
  };
}
