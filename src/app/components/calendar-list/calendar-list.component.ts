import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-calendar-list',
  templateUrl: './calendar-list.component.html',
  styleUrls: ['./calendar-list.component.css']
})
export class CalendarListComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private router: Router
) { }
  
  teams;
  selectedTeam;

  ngOnInit() {
    this.teams = this.dataService.teams;
    this.selectedTeam = this.teams[Object.keys(this.teams)[0]];

    this.dataService.teamChange.subscribe(data => {
      this.teams.forEach(team => {
        if (data.team == team.uuid) team = data.team;
      })
    })
  }

  viewTeam(team): void {
    if (team.name == 'Personal') this.router.navigateByUrl(`team/personal`);
    else this.router.navigateByUrl(`team/${team.uuid}`);
  }

  selectCalendar(): void {
    this.dataService.selectCalendar(this.selectedTeam.uuid);
  }
}
