import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-background',
  templateUrl: './background.component.html',
  styleUrls: ['./background.component.css']
})
export class BackgroundComponent implements OnInit {

  numberOfBackgrounds = 10;
  src: string = `../../assets/images/bg${Math.floor(Math.random() * Math.floor(this.numberOfBackgrounds)) + 1}.jpg`
  constructor() {
  }
  ngOnInit() {}

}
