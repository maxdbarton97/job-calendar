import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {
  session: boolean = false;

  constructor(
    router: Router,
    route: ActivatedRoute,
    dataService: DataService
  ) {
    router.events.forEach(event => {
      if (event instanceof NavigationEnd
      && route.root.firstChild.snapshot.data) {
        const sessionlessView = !!route.root.firstChild.snapshot.data['sessionless'];
        this.session = dataService.session;
        if (this.session && sessionlessView) router.navigateByUrl('home');
        if (!this.session && !sessionlessView) router.navigateByUrl('login');
      }
    });
  }

  ngOnInit() {
    // read for route change and redirect if session isn't correct
  }
}
