import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from 'app/services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private dataService: DataService,
    private router: Router
    ) { }

  user: any = {};
  picture: string;
  ngOnInit() {
    this.dataService.userChange.subscribe(user => {
      this.user = user;
      this.picture = user.picture;
    });
    this.dataService.getProfile();
  };

  logout(): void {
    this.http.post('api/auth/logout', {}).subscribe(() => {
      this.dataService.session = false;
      this.router.navigateByUrl('login');
    });
  };
}
