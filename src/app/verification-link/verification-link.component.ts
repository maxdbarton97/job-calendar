import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpUrlEncodingCodec } from '@angular/common/http';
import { registerContentQuery } from '@angular/core/src/render3';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-verification-link',
  templateUrl: './verification-link.component.html',
  styleUrls: ['./verification-link.component.css']
})
export class VerificationLinkComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private activatedRoute: ActivatedRoute
  ) { }

  status: string = 'Verifying...'
  spinner: boolean = true;
  home: boolean;

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.http.post('api/auth/verification/check-code', {
        verificationCode: params.verificationCode
      }).subscribe(response => {
        if (response['exists'] === false) {
          this.spinner = false;
          this.home = false;
          return this.status = 
            `The URL is not valid. Please double check the link, or register 
            again (you will be able to use the same email).`
        }
        if (response['verified'] === true) {
          this.spinner = false;
          this.home = false;
          return this.status = 
            `Your account has already been verified.`
        }

        // double check it's not verified and then complete verification
        if (response['verified'] === false) {
          this.http.post('api/auth/verification/verify-user', {
            verificationCode: params.verificationCode
          }).subscribe(response => {  
            this.home = true;
            return this.status = `Your account has successfully been verified!`
          });
        }
      })
    });
  }
}
