import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, NgForm } from '@angular/forms';

import { DataService } from '../services/data.service';
import { UUID } from 'angular2-uuid';
import { Job } from 'app/mods/job';
import { DateAdapter } from '@angular/material';

@Component({
  selector: 'app-job-entry',
  templateUrl: './job-entry.component.html',
  styleUrls: ['./job-entry.component.css']
})
  
export class JobEntryComponent implements OnInit {
  constructor(
    private dataService: DataService,
    private dateAdapter: DateAdapter<Date>
  ) {
    this.dateAdapter.setLocale('es');
  };
  
  isOpen: boolean = false;
  users: Object[];
  places: string[];
  uuid: any;
  job;
  fieldEntry: boolean;
  userUuid: string;
  user: any;
  existingJob;

  ngOnInit() { 
    this.dataService.userChange.subscribe(user => {
      this.userUuid = user.uuid;
    })

    this.dataService.getProfile();

    this.users = this.dataService.getUsers();
    this.places = this.dataService.getPlaces();
    this.getUser();

    this.dataService.usersChange.subscribe(value => {
      this.users = value.users;
      this.getUser();
    })

    this.dataService.placesChange.subscribe(value => {
      this.places = value.places;
    })

    this.dataService.jobEntryChange.subscribe(value => {
      this.isOpen = value.isOpen;
      if (!this.isOpen) return;

      this.resetForm();
      this.cancelFieldEntry();

      let job = { date: value.date }
      if (value.job) {
        job = value.job;
        this.existingJob = true;
      }

      // clean up custom fields
      Object.keys(this.entryForm.controls).forEach(value => {
        if (value.includes('(Custom)')) {
          this.entryForm.removeControl(value);
        }
      });
          
      Object.keys(job).forEach((val, index) => {
        if (val == 'date') {
          this.entryForm.controls.date.setValue(job[Object.keys(job)[index]]);
          this.entryForm.controls.time.setValue(
            // hours
            (job.date.getHours().toString().length < 2 ? '0' : '')
            + job.date.getHours() + ':' +
            // minutes
            (job.date.getMinutes().toString().length < 2 ? '0' : '') 
            + job.date.getMinutes()
          )
          return;
        }

        if (val == 'customs') {
          // apparantly you can't add a control if you've removed one with the 
          // same name within the same lifecylce hook so bullshit setTimeout ;D 
          setTimeout(() => { job[Object.keys(job)[index]].forEach(field => {
            this.entryForm.addControl(
              field.name,
              new FormControl('', Validators.required)
            )
            this.entryForm.controls[field.name].setValue(field.value);
          });
          return})
        }

        if (val == 'archived' || val == 'completed') return;    
        if (val == 'uuid') this.uuid = job[Object.keys(job)[index]];
        else if (this.entryForm.controls[val]) {
          this.entryForm.controls[val].setValue(job[Object.keys(job)[index]]);
        }
      });  
    });
  };

  getUser(): void {
    this.users.forEach(user => {
      if (user['uuid'] == this.userUuid) {
        this.user = user;
      }
    })
  }
  
  times: string[] = [
    '00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00',
    '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', 
    '14:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00',
    '22:30', '23:00', '23:30'
  ];

  colors: string[] = [
    '#D50000',
    '#C51162',
    '#AA00FF',
    '#6200EA',
    '#2962FF',
    '#E65100',
    '#43A047',
    '#A1887F',
    '#757575',
    '#000000'
  ];

  minDate: Date = new Date();
  
  entryForm: FormGroup = new FormGroup({
    'date': new FormControl('', [Validators.required]),
    'time': new FormControl('', [Validators.required]),
    'title': new FormControl('', [Validators.required]),
    'description': new FormControl('', [Validators.required]),
    'users': new FormControl('', [Validators.required]),
    'place': new FormControl('', []),
    'location': new FormControl('', [Validators.required]),
    'color': new FormControl('', [Validators.required])
  })
  
  @ViewChild('formDirective') public formDirective: NgForm;

  newFieldEntryForm: FormGroup = new FormGroup({
    "name": new FormControl('', [Validators.required]),
    "value": new FormControl('', [Validators.required])
  })

  @ViewChild('newFieldEntryFormDirective') public newFieldEntryFormDirective: NgForm;
  
  submit(): void {
    const customs = [];
    this.keys(this.entryForm.value).forEach(control => {
      if (control.includes("(Custom)")) {
        customs.push({
          name: control,
          value: this.entryForm.value[control]
        });
      };
    });
    
    let job = {
      uuid: this.uuid ? this.uuid : UUID.UUID(),
      date: new Date(
        this.entryForm.value.date.getFullYear(), 
        this.entryForm.value.date.getMonth(),
        this.entryForm.value.date.getDate(),
        this.entryForm.value.time.slice(0, 2),
        this.entryForm.value.time.slice(3, 5)
      ),
      title: this.entryForm.value.title,
      description: this.entryForm.value.description,
      users: this.entryForm.value.users,
      place: this.entryForm.value.place,
      location: this.entryForm.value.location,
      color: this.entryForm.value.color,
      archived: false,
      completed: false,
      customs: customs
    };

    if (this.existingJob) {
      this.dataService.editJob(job)
      this.existingJob = false;
    } else {
      this.dataService.createJob(job);
    }

    // resets
    this.uuid = false;
    this.resetForm();
    this.dataService.closeJobEntry();
  };
  
  resetForm() {
    this.formDirective.resetForm();
    this.entryForm.reset();
    this.fieldEntry = false;
  };
  
  close() {
    this.dataService.closeJobEntry();
  }

  selectColor(color): void {
    this.entryForm.controls.color.setValue(color);
  }

  initFieldEntry() {
    this.fieldEntry = true;
  }

  cancelFieldEntry() {
    this.newFieldEntryFormDirective.resetForm();
    this.newFieldEntryForm.reset();
    this.fieldEntry = false;
  }

  keys(obj) {
    return Object.keys(obj);
  }

  createFieldEntry() {
    this.entryForm.addControl(
      this.newFieldEntryForm.controls.name.value + ' (Custom)',
      new FormControl(this.newFieldEntryForm.value.value, Validators.required)
    );
    this.cancelFieldEntry();
  }

  removeCustomField(custom) {
    this.entryForm.removeControl(custom);
  }
}
