export class Job {
    uuid: string;
    date: Date;
    title: string;
    description: string;
    place: string;
    users: object[];
    location: string;
    color: string;
    archived: boolean;
    completed: boolean;
    customs: object[];
}

