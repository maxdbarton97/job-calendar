import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, Validators, AbstractControl, AsyncValidatorFn, 
         ValidationErrors, FormGroup } from '@angular/forms';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  constructor(
    private http: HttpClient,
    private router: Router
  ) { };

  ngOnInit() {

    setInterval(() => this.spinner = !this.spinner, 2000);
  };

  hide: boolean = true;
  register: boolean = false;
  spinner: boolean = false;

  //
  // custom async validators
  //

  existingEmailValidator(): AsyncValidatorFn {
    return (email: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
      this.register = false;
      return this.http.post('api/auth/register/email-check', { email: email.value.toLowerCase() })
        .map((data) => {
          this.checkInputs();
          if (data['error']) {
            return { 'exists': true };
          }
          return (data['exists'] == false) ? null : { 'exists': true }
        })
    }
  };

  existingUsernameValidator(): AsyncValidatorFn {
    return (username: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
      this.register = false;
      return this.http.post('api/auth/register/username-check', { username: username.value })
        .map((data) => {
          this.checkInputs();
          if (data['errors']) {
            return { 'exists': true }
          }
          return (data['exists'] == false) ? null : { 'exists': true }
        })
    }
  };

  // form control objects to aid validation
  registerForm: FormGroup = new FormGroup({
    'email': new FormControl('', [Validators.required, Validators.email], [this.existingEmailValidator()]),
    'forename': new FormControl('Dave', [Validators.required, Validators.maxLength(10), Validators.pattern(/^([^0-9]*)$/)]),
    'surnames': new FormControl('', [Validators.required, Validators.maxLength(20), Validators.pattern(/^([^0-9]*)$/)]),
    'username': new FormControl('', [Validators.required, Validators.maxLength(16), Validators.pattern('^[a-z0-9_]*$')], [this.existingUsernameValidator()]),
    'password': new FormControl('', [Validators.required, Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$")])
  })

  //
  // register a new user
  //

  checkInputs(): void {
    setTimeout(() => {
      if (!this.registerForm.controls['email'].errors &&
        !this.registerForm.controls['forename'].errors &&
        !this.registerForm.controls['surnames'].errors &&
        !this.registerForm.controls['username'].errors &&
        !this.registerForm.controls['password'].errors
      ) {
        this.register = true
      }
      else this.register = false;
    }, 50);
  };

  registerUser(): void {
    this.spinner = true;
    // check for no errors
    if (!this.registerForm.controls['email'].errors &&
      !this.registerForm.controls['forename'].errors &&
      !this.registerForm.controls['surnames'].errors &&
      !this.registerForm.controls['username'].errors &&
      !this.registerForm.controls['password'].errors
    ) {
      this.http.post('api/auth/register', {
        email: this.registerForm.controls['email'].value.toLowerCase(),
        forename: this.registerForm.controls['forename'].value,
        surnames: this.registerForm.controls['surnames'].value,
        username: this.registerForm.controls['username'].value,
        password: this.registerForm.controls['password'].value,
      }).subscribe(data => {
        this.router.navigate(['verify-account']);
      }, err => {
        console.error(err)
      });
    }
  };

}