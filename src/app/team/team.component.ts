import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { DataService } from 'app/services/data.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.less']
})
export class TeamComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private dataService: DataService,
    private router: Router,
    ) { }

  users: Object[];
  teamUuid: string;
  selectedUser: Object;
  teamName: string;
  overview: Boolean;


  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.teamUuid = params.teamUuid;
      this.users = this.dataService.getUsers(this.teamUuid);
      this.dataService.teams.forEach(value => {
        if (value.uuid == this.teamUuid) {
          this.teamName = value.name;
        }
      })
      if (this.router.url === `/team/${this.teamUuid}`) this.overview = true;
      this.users.forEach(value => {
        if (this.router.url.includes(value['uuid'])) this.selectedUser = value;
      });
    });
  }

  selectUser(user): void {
    this.overview = false;
    this.selectedUser = user;
    this.router.navigate([`/team/${this.teamUuid}/users/${user.uuid}`]);
  }

  viewTeam(): void {
    this.overview = true;
    this.selectedUser = null;
    this.router.navigate([`/team/${this.teamUuid}`]);
  }
}
