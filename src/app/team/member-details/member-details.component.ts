import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DataService } from 'app/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.css']
})
export class MemberDetailsComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  @ViewChild("userNameInput") userNameInput: ElementRef;

  userUuid;
  teamUuid: string;
  viewedUserUuid: string;
  me: Object;
  users: Object[];
  user: Object = {};
  isEditingName: boolean;
  isEditingPermissions: boolean;
  userForm: FormGroup;
  permissions: string[] = [
    'worker',
    'organiser',
    'leader',
    'owner'
  ]

  ngOnInit() {

    this.dataService.getProfile();
    this.dataService.userChange.subscribe(user => {
      this.userUuid = user.uuid;
    })


    this.activatedRoute.params.subscribe(params => {
      this.teamUuid = this.activatedRoute.parent.params['value'].teamUuid;
      this.viewedUserUuid = params.userUuid;
      this.getUserFromUsers(this.dataService.getUsers(this.teamUuid));

      this.dataService.usersChange.subscribe(value => {
        this.getUserFromUsers(value.users);
      });
    });

    this.userForm = new FormGroup({
      "name": new FormControl('', [Validators.required, Validators.maxLength(24)]),
      "permissions": new FormControl(this.user['permissions'][0])
    })

    // reset the state if changing users (new params..)
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  getUserFromUsers(users) {
    users.forEach(value => {
      if (value['uuid'] == this.viewedUserUuid) {
        this.user = value;
      }
      if (value['uuid'] == this.userUuid) {
        this.me = value;
      }
    })
  }

  editUserName(): void {
    this.isEditingName = true;
    this.userForm.controls['name'].setValue(this.user['name']);
    setTimeout(() => {
      this.userNameInput.nativeElement.focus();
    })
  }

  saveUserName(): void {
    this.isEditingName = false;
    this.dataService.editUserAlias(this.teamUuid, this.userForm.controls['name'].value, this.user)
  }

  editUserPermissions(): void {
    this.isEditingPermissions = true;
  }

  saveUserPermissions(): void {
    this.isEditingPermissions = false;
    this.dataService.editUserPermissions(this.teamUuid, this.userForm.controls['permissions'].value, this.user['uuid']);
  }
  
  cancelUserPermissions(): void {
    this.isEditingPermissions = false;
    this.userForm.controls['permissions'].setValue(this.user['permissions'][0])
  }
}
