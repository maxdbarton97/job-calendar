import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'app/services/data.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css']
})
export class TeamDetailsComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private dataService: DataService,
  ) { }

  @ViewChild("teamNameInput") teamNameInput: ElementRef;
  @ViewChild("placeNameInput") placeNameInput: ElementRef;

  teamUuid: string;
  users: Object[];
  teamName: string;
  team;
  userTypes: Object;
  isEditingName: boolean; 
  editPlace: any;
  placeInput: boolean;
  jobTypes: Object = {
    "Total" : 0,
    "Completed" : 0,
    "Active": 0,
    "Overdue": 0
  };
  teamNameForm: FormGroup;
  placeForm: FormGroup;
  Object: Object = Object;
  userUuid: string;
  me: Object;

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.teamUuid = params.teamUuid;

      this.dataService.getProfile();
      this.dataService.userChange.subscribe(user => {
        this.userUuid = user.uuid;
      })
      
      this.users = this.dataService.getUsers(this.teamUuid);
      this.users.forEach(user => {
        if (user['uuid'] == this.userUuid) {
          this.me = user;
        }
      });
      this.dataService.teams.forEach(value => {
        if (value['uuid'] == this.teamUuid) {
          this.teamName = value['name'];
        }
      })

      this.team = this.dataService.getTeam(this.teamUuid);
      this.userTypes = {
        "Workers": this.team['users'].filter(user => user.permissions.includes('worker') && !user.permissions.includes('organiser')),
        "Organisers": this.team['users'].filter(user => user.permissions.includes('organiser') && !user.permissions.includes('leader')),
        "Leaders": this.team['users'].filter(user => user.permissions.includes('leader') && !user.permissions.includes('owner')),
        "Owners": this.team['users'].filter(user => user.permissions.includes('owner'))
      }

      for (let month in this.team['jobs']) {
      for (let day in this.team['jobs'][month]) {
        this.jobTypes['Total'] += this.team['jobs'][month][day].length;
        for (let job in this.team['jobs'][month][day])
          if (this.team['jobs'][month][day][job].completed) this.jobTypes['Completed'] += 1;
          else if (this.team['jobs'][month][day][job].date < new Date()) this.jobTypes['Overdue'] += 1;
          else this.jobTypes['Active'] += 1;
        }
      }

      this.dataService.placesChange.subscribe(value => {
        // only if updating the current teams place list..
        if (value.teamUuid == this.team.uuid) {
          this.team.places = value.places;
        } 
      })
    })

    this.teamNameForm = new FormGroup({
      "teamName": new FormControl(this.team['name'], [Validators.required, Validators.maxLength(24)])
    })

    this.placeForm = new FormGroup({
      "placeName": new FormControl('', [Validators.required, Validators.maxLength(24)])
    })
  }

  editTeamName(): void {
    this.isEditingName = true;
    setTimeout(() => {
      this.teamNameInput.nativeElement.focus();
    });
  }

  saveTeamName(): void {
    this.dataService.updateTeamName(this.teamUuid, this.teamNameForm.value.teamName);
    this.isEditingName = false;
  }

  cancelEditTeamName(): void {
    this.teamNameForm.controls.teamName = this.team['name'];
    this.isEditingName = false;
  }

  addplace(): void {
    this.placeInput = true;
  }

  saveplace(): void {
    if (!!this.editPlace) {
      this.dataService.editPlaceName(this.teamUuid, this.placeForm.value.placeName, this.editPlace);
      this.editPlace = false;
    }  else {
      this.dataService.addplace(this.teamUuid, this.placeForm.value.placeName);
    }
    this.placeInput = false;
    this.placeForm.reset();
  }

  deleteplace(): void {
    this.dataService.deleteplace(this.editPlace.uuid, this.editPlace.team);
    this.editPlace = false;
    this.placeInput = false;
    this.placeForm.reset();
  }

  cancelAddplace(): void {
    this.placeInput = false;
  }

  openEditplace(place): void {
    this.editPlace = place;
    this.placeInput = true;
    this.placeForm.controls['placeName'].setValue(place.name);
  }
}
