import { Injectable, EventEmitter, Output } from '@angular/core';

import { DatePacket } from '../mods/date-packet';

@Injectable({
  providedIn: 'root'
})

export class DateService {

  constructor() { };
  
  monthMap = {
    1: 'February ',
    0: 'January ',
    2: 'March ',
    4: 'May ',
    3: 'April ',
    5: 'June ',
    6: 'July ',
    7: 'August ',
    8: 'September ',
    9: 'October ',
    10: 'November ',
    11: 'December '
  };
  
  currentDate: DatePacket = this.createDatePacket(new Date(new Date().getFullYear(), new Date().getMonth()));
  
  @Output() change: EventEmitter<DatePacket> = new EventEmitter();
  
  set selectedDate(datePacket: DatePacket) {
    this.currentDate = datePacket;
    this.change.emit(this.currentDate);
  }
  
  get selectedDate(): DatePacket {
    return this.currentDate;
  }
  
  getButtonDates(): DatePacket[] {
    const dates = [];
    for (let x = -1; x < 8; x++) {
      let tempDate: DatePacket;
      if ((this.currentDate.date.getMonth() + x) > 11) tempDate = this.createDatePacket(new Date(this.currentDate.date.getFullYear()+1, this.currentDate.date.getMonth() + (x - 12), 1));
      else tempDate = this.createDatePacket(new Date(this.currentDate.date.getFullYear(), this.currentDate.date.getMonth() + x, 1));
      dates.push(tempDate);
    };
    return dates;
  };
  
  getMonthLength(date): number {
    return new Date(date.getFullYear(), date.getMonth()+1, 0).getDate();
  }
  
  createDatePacket(date: Date): DatePacket {
    return {
      date: date,
      text: (this.monthMap[date.getMonth()] + date.getFullYear().toString())
    }
  };

}
