import { Injectable, EventEmitter, Output } from '@angular/core';

import { DateService } from './date.service';
import { UUID } from 'angular2-uuid';
import randomWords  from '../../../node_modules/random-words/index.js'
import { HttpClient } from '@angular/common/http';

const permissionsList = [
  'owner',
  'leader',
  'organiser',
  'worker'
]

@Injectable({
  providedIn: 'root'
})

export class DataService {

  data = {
    user: null,
    teams: [
      {
        name: 'Personal',
        uuid: 'xab3c37e1-36c0-4bb9-9b5b-e5695d4a9f28',
        users: [{alias: 'Max', uuid: 'xab3c37e1-36c0-4bb9-9b5b-e5695d4a9f28'}],
        places: [{ name: 'Home', uuid: '47580677-01bd-467f-b220-7540fef22ceb', team: 'xab3c37e1-36c0-4bb9-9b5b-e5695d4a9f28'}],
        jobs: [],
        tags: [
          { name: 'High Priority', color: '#f44336' },
          { name: 'All Day Job', color: '#FF9800' }
        ],
      },
      {
        name: 'The Simpsons',
        uuid: "97bfd782-80db-46be-b9db-a95aff3aa875",
        users: [
          { 
            alias: 'Max', 
            uuid: 'xab3c37e1-36c0-4bb9-9b5b-e5695d4a9f28',
            permissions: [
              'owner',
              'leader',
              'organiser',
              'worker'
            ]
          },
          {
            alias: 'Homer',
            uuid: '80d89d47-2a41-4762-9832-bbdd995b4bad',
            permissions: [
              'owner',
              'leader',
              'organiser',
              'worker'
            ]
          },
          { 
            alias: 'Marge', 
            uuid: '8cca995a-28d1-493c-a3b7-0d99e82ccaea',
            permissions: [
              'leader',
              'organiser',
              'worker'
            ]
          },
          { 
            alias: 'Bart', 
            uuid: 'b9a9a645-d3e0-4d04-9617-cd283dcd400c',
            permissions: [
              'worker'
            ]
          },
          { 
            alias: 'Lisa', 
            uuid: 'fff7b770-aad8-4156-87c8-a22355c4a3b6',
            permissions: [
              'organiser',
              'worker'
            ]
          },
          { 
            alias: 'Maggie', 
            uuid: '48e8ddd1-a085-4789-a31d-bf4e158d0c6e',
            permissions: [
              'worker'
            ]
          }
        ],
        places: [{
          name: 'Buzz Cola',
          uuid: '21d90937-9291-4782-a7ff-22807fe4f204',
          team: '97bfd782-80db-46be-b9db-a95aff3aa875'
        },
        {
          name: 'Elementary School',
          uuid: 'b992bd4c-7dcb-46f7-b65b-bd42316224d5',
          team: '97bfd782-80db-46be-b9db-a95aff3aa875'
        },
        {
          name: "Moe's Tavern",
          uuid: '634f10a1-79f0-4f16-87d2-b78c24d9b612',
          team: '97bfd782-80db-46be-b9db-a95aff3aa875'
        },
        {
          name: 'Duff Brewery',
          uuid: '27a8d705-d5d1-4255-abf5-aefd44b55ce5',
          team: '97bfd782-80db-46be-b9db-a95aff3aa875'
        },
        {
          name: 'Springfield Hospital',
          uuid: '11383240-addf-41ed-9dfc-559eb9fccb59',
          team: '97bfd782-80db-46be-b9db-a95aff3aa875'
        }],
        jobs: [],
        tags: [
          { name: 'High Priority', color: '#f44336' },
          { name: 'All Day Job', color: '#FF9800' }
        ],
      },
    ],
    state: {
      filters: {
        users: [],
        places: [],
        tags: [],
        status: [],
        activity: []
      },
      selectedTeamUuid: 'xab3c37e1-36c0-4bb9-9b5b-e5695d4a9f28',
      jobEntry: false,
      jobView: false,
      session: false
    }
  }

  // hard data...
  jobUuidMap: object = {}
  colors: string[] = [
    '#C51162',
    '#AA00FF',
    '#6200EA',
    '#2962FF',
    '#E65100',
    '#43A047',
    '#A1887F',
    '#757575',
    '#000000'
  ];

  @Output()
    userChange: EventEmitter<Object> = new EventEmitter();
    usersChange: EventEmitter<Object> = new EventEmitter();
    placesChange: EventEmitter<Object> = new EventEmitter();
    filtersChange: EventEmitter<Object> = new EventEmitter();
    jobChange: EventEmitter<Object> = new EventEmitter();
    change: EventEmitter<Object> = new EventEmitter();
    jobEntryChange: EventEmitter<Object> = new EventEmitter();
    jobViewChange: EventEmitter<Object> = new EventEmitter();
    tagsChange: EventEmitter<Object> = new EventEmitter();
    selectedTeamChange: EventEmitter<Object> = new EventEmitter();
    teamChange: EventEmitter<Object> = new EventEmitter();

  constructor(
    private dateService: DateService,
    private http: HttpClient
  ) {
    //generate some random tasks for all selectable months
    // -------------------------------------------
    for (let x = -1; x < 8; x++) {
      for (let n = -5; n < 37; n++) {  
        this.jobGenerator(n, new Date(new Date().getFullYear(), new Date().getMonth() + x));
        // this.jobGenerator(n, new Date(new Date().getFullYear(), new Date().getMonth() + x), "97bfd782-80db-46be-b9db-a95aff3aa875");
      }
    }
    // --------------------------------------------


    // get all the data!
    





  };

  jobGenerator(number, givenDate, teamUuid?) {
    if (!teamUuid) teamUuid = this.data.state.selectedTeamUuid;
    let date = number;
    new Array(Math.floor(Math.random() * 3) + 0).fill(0, 0, 5).map(val => {
      const uuid = UUID.UUID();
      const date_ = new Date(
        givenDate.getFullYear(),
        givenDate.getMonth(),
        date,
        Math.floor(Math.random() * 23) - 0
      );

      this.jobUuidMap[uuid] = date_;

      //get team
      let team_
      this.data.teams.forEach(team => {
        if (team.uuid == teamUuid) team_ = team;
      })

      this.createJob({
        uuid: uuid,
        date: date_,
        title: randomWords({exactly: 1, maxLength: 6, wordsPerString: 3 }),
        description: 'Description..',
        place: team_.places[Math.floor(Math.random()*team_.places.length)],
        users: [team_.users[Math.floor(Math.random()*team_.users.length)]],
        color: this.colors[Math.floor(Math.random()*this.colors.length)],
        location: 'Location',
        archived: Math.random() >= 0.5,
        completed: Math.random() >= 0.5,
        customs: [
          { name: 'Field! (Custom)', value: 'Example!' },
          { name: 'Creamy (Custom)', value: 'Banter!' }
        ]
      }, teamUuid);
    });
  }
  
  getJobs(date: Date, teamUuid?): Object[] {
    const jobs = [];
    if (!teamUuid) teamUuid = this.data.state.selectedTeamUuid;
    this.data.teams.forEach(team => {
      if (team.uuid == teamUuid) {
        for (let job of team.jobs) {
          // convert to string to make compareable
          if (new Date(date.getFullYear(), date.getMonth(), date.getDate()).toString()
          == new Date(job.date.getFullYear(), job.date.getMonth(), job.date.getDate()).toString()) {
            jobs.push(job);
          }
        }
      }
    });
    return jobs;
   }

   getAllJobs(teamUuid?): Object[] {
    let jobs;
    if (!teamUuid) teamUuid = this.data.state.selectedTeamUuid;
    this.data.teams.forEach(team => {
      if (team.uuid == teamUuid) {
        jobs = team.jobs
      }
    })
    return jobs;
   }
  
  getUsers(teamUuid?): string[] {
    if (!teamUuid) teamUuid = this.data.state.selectedTeamUuid;
    let users;
    this.data.teams.forEach(team => {
      if (team.uuid == teamUuid) users = team.users;
    })
    return users;
  };

  getPlaces(teamUuid?): string[] {
    if (!teamUuid) teamUuid = this.data.state.selectedTeamUuid;
    let places;
    this.data.teams.forEach(team => {
      if (team.uuid == teamUuid) places = team.places;
    })
    return places;
  };
  
  getTags(teamUuid?): Object[] {
    if (!teamUuid) teamUuid = this.data.state.selectedTeamUuid;
    let tags;
    this.data.teams.forEach(team => {
      if (team.uuid == teamUuid) tags = team.tags;
    })
    return tags;
  };



  editJob(job): void { 
    this.data.teams.forEach(team => {
      if (team.uuid == this.data.state.selectedTeamUuid) {
        for (let job_ of team.jobs) {
          if (job.uuid == job_.uuid) {
            job_ = job;
            this.jobChange.emit();
          }
        }
      }
    })
  }

  createJob(job, teamUuid?): void {
    if (!teamUuid) teamUuid = this.data.state.selectedTeamUuid;
    for (let team of this.data.teams) {
      if (team.uuid == teamUuid) {
        team.jobs.push(job);
        this.jobChange.emit();
      }
    }
  };
  
  createplace(place, teamUuid?): void {
    if (!teamUuid) teamUuid = this.data.state.selectedTeamUuid;
    this.data.teams.forEach(team => {
      if (team.uuid == teamUuid) team.places.push(place);
    });
  };
  
  openJobEntry(date, job): void {
    this.data.state.jobEntry = job;
    this.jobEntryChange.emit({
      isOpen: true, date, job
    });
  }

  closeJobEntry(): void {
    this.data.state.jobEntry = false;
    this.jobEntryChange.emit({});
  }

  openJobView(job): void {
    // load job or just true to open blank
    this.data.state.jobView = job ? job: true;
    this.jobViewChange.emit(this.data.state.jobView);
  }

  closeJobView() {
    this.data.state.jobView = false;
    this.jobViewChange.emit(false);
  }

  archive(job) {
    this.setJobProp(job, 'archived', true);
  }

  complete(job) {
    this.setJobProp(job, 'completed', true);
  }

  reopen(job) {
    this.setJobProp(job, 'completed', false);
  }

  setJobProp(job, prop, val, teamUuid?) {
    if (!teamUuid) teamUuid = this.data.state.selectedTeamUuid;
    this.data.teams.forEach(team => {
      if (team.uuid == teamUuid) {
        for (let job_ of team.jobs) {
          if (job_.uuid == job.uuid) {
            job_[prop] = val;
            if (prop == 'completed') job_.overdue = !val;
            this.jobChange.emit();
          }
        }
      };
    })
  }

  updateFilter(filterType, filter) {
    this.data.state.filters[filterType] = filter;
    this.filtersChange.emit(this.data.state.filters)
  }

  selectCalendar(teamUuid): void {
    this.data.state.selectedTeamUuid = teamUuid;
    this.data.teams.forEach(team => {
      if (team.uuid == teamUuid) {
        this.data.state.selectedTeamUuid = teamUuid;
        this.usersChange.emit({teamUuid: this.data.state.selectedTeamUuid, users: team.users});
        this.placesChange.emit({teamUuid, places: team.places});
        this.selectedTeamChange.emit(teamUuid);
        // this.tagsChange.emit(team.tags);
      }
    })
  }

  get selectedTeamUuid(): string {
    return this.data.state.selectedTeamUuid;
  }

  get filters(): any {
    return this.data.state.filters;
  }

  get teams() {
    return this.data.teams;
  }

  getTeam(teamUuid): Object {
    let team;
    this.data.teams.forEach(t => {
      if (t.uuid == teamUuid) team = t;
    })
    return team;
  }

  updateTeamName(teamUuid, name): void {
    this.data.teams.forEach(team => {
      if (team.uuid == teamUuid) {
        team.name = name;
        this.teamChange.emit({ team });

      }
    })
  }

  addplace(teamUuid, name): void {
    this.data.teams.forEach(team => {
      if (team.uuid == teamUuid) {
        team.places.push({ name, uuid: UUID.UUID(), team: teamUuid });
        this.placesChange.emit({teamUuid, places: team.places});
      }
    })
  }

  editPlaceName(teamUuid, name, place): void {
    this.data.teams.forEach(team => {
      if(team.uuid == teamUuid) {
        team.places.forEach(place_ => {
          if (place_ == place) place_.name = name
          this.placesChange.emit({teamUuid, places: team.places});
        })
      }
    });
  }

  deleteplace(placeUuid, teamUuid): void {
    this.data.teams.forEach(team => {
      if (team.uuid == teamUuid) {
        team.places.forEach((place_, index) => {
          if (place_.uuid == placeUuid) {
            team.places.splice(index, 1)
            this.placesChange.emit({ teamUuid, places: team.places });
          }
        })
      }  
    })    
  }
  

  editUserAlias(teamUuid, name, userUuid): void {
    this.data.teams.forEach(team => {
      if (team.uuid == teamUuid) {
        for (let user of team.users) {
          if (user.uuid == userUuid) {
            user['name'] = name;
            this.usersChange.emit({
              teamUuid, users: this.data.teams[teamUuid].users
            })
          }
        }
      }
    })
  }
  editUserPermissions(teamUuid, permissions, userUuid) {
    // create permissions array
    const newPermissions = permissionsList.slice(permissionsList.indexOf(permissions), permissionsList.length);
    this.data.teams.forEach(team => {
      if (team.uuid == teamUuid) {
        for (let user of team.users) {
          if (user == userUuid) {
            user['permissions'] = newPermissions;
            this.usersChange.emit({teamUuid, users: team.users});
          }
        }
      }
    })
  }

  updateProfile(user): any {
    this.put('user/update', user).then((data: any) => {
        this.data.user = data.user;
        this.userChange.emit(this.data.user);
    })
  };

  get userUuid(): string {
    return this.data.user.uuid;
  };

  set session(session: boolean) {
    this.data.state.session = session;
  };

  get session(): boolean {
    return this.data.state.session;
  };

  getProfile(): any {
    if (this.data.user || this.data.user == 'getting' || !this.data.state.session) {
      return this.userChange.emit(this.data.user);
    }
    this.data.user = 'getting';
    this.get('user').then((data: any) => {
      this.data.user = data.user;
      this.userChange.emit(this.data.user);
    }, error => {
      this.apiError(error) // pass a custom message..
    })
  };

  get(url) {
    return new Promise((res, rej) => {
      this.http.get(`api/${url}`).subscribe((data: any) => {
        if (data.error) rej(data.error);
        else res(data);
      }, error => rej(error))
    })
  }

  put(url, params) {
    return new Promise((res, rej) => {
      this.http.put(`api/${url}`, params).subscribe((data: any) => {
        if (data.error) rej(data.error);
        else res(data);
      }, error => rej(error))
    })
  }

  apiError(error) {
    alert(JSON.stringify(error));
    // emit error event here..
  }
}