import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { DataService } from 'app/services/data.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  constructor(
    private http: HttpClient,
    private router: Router,
    private dataService: DataService
    ) {};

  ngOnInit() {
  };
  
  userDetails = {
      email: '',
      password: ''
  };
  
  error: string = '';
  
  login(): void {
      this.http.post('api/auth/login', {
        email: this.userDetails.email,
        password: this.userDetails.password
      }).subscribe(data => {
          if (data['active'] === false) this.error = 'Please check your email and verify your account.';
          if (data['attempt'] === false) this.error = 'Username and/or password incorrect. Please try again.';
          if (data['attempt'] === true) {
            this.dataService.session = true;
            this.router.navigateByUrl('home');
          }
      });
  }

}
